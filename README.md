**Desarrolla Aplicaciones Web usando Bases de Datos**
**Coronel Aispuro Sebastian**
**Semestre No. 5**

- Práctica #1 - 02/09/2022 - Práctica de ejemplo
Commit: e914044cb30d3aa5e98fae5cbf9670d6d8b6376c
Archivo: https://gitlab.com/sebastiancoron15/desarolla_aplicaciones_web/-/blob/main/practica_ejemplo.html

- Práctica #2 - 09/09/2022 - Práctica JavaScript
Commit: 82599fa8e6c547c227f3bbe2ff06fec5ddff496b
Archivo: https://gitlab.com/sebastiancoron15/desarolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Práctica #3 - 15/09/2022 - Práctica web con bases de datos parte 1
Commit: 6a8910a3ae8ada10d8e31ad00bffe57c053925c3

- Práctica #4 - 19/09/2022 - Práctica web con bases de datos - Vista de consultas de datos 
Commit: ccfd43778e5a5bf1c10659280bc50d3380a677ca

- Práctica #5 - 22/09/2022 - Práctica web con bases de datos - Lista de Registro de datos
Commit: b6b5005ae548633eaebb8ec28742b5740507c9f9

- Práctica #6 - 23/09/2022 - Práctica web con bases de datos - Conexión con base de datos
Commit: 7a472751c974dcea81646fc5349f5acbd8e6af0e